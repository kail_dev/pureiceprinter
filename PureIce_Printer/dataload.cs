﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PureIce_Printer
{
    public class dataload
    {

        public virtual bool UpdateStatus(int id)
        {
            Entities ent = new Entities();
            ZZSPE_PRINT plcRecord = ent.ZZSPE_PRINT.Where(e => e.ID == id).First();
            plcRecord.STATUS = 2;
            return ent.SaveChanges() > 0;


        }

        public virtual List<ZZSPE_PRINT> ToRePrint(int id)
        {
            Entities en = new Entities();
            List<ZZSPE_PRINT> Toprint = en.ZZSPE_PRINT.Where(e => e.ID == id).ToList();
            return Toprint;
        }

        public virtual bool Print(ZZSPE_PRINT e)
        {
            Entities ent = new Entities();
            var plcRecord = ent.Set<ZZSPE_PRINT>();

            plcRecord.Add(new ZZSPE_PRINT() { DOK_GIDNumer = e.DOK_GIDNumer, DOK_GIDTyp = e.DOK_GIDTyp, DOK_NAZ = e.DOK_NAZ, ID_PRINTER = e.ID_PRINTER, NR_PALETY = e.NR_PALETY, TWR_CECHA = e.TWR_CECHA, TWR_DATA = e.TWR_DATA, TWR_DATA2 = e.TWR_DATA2, TWR_DATA_DE = e.TWR_DATA_DE, TWR_DATA_DE1 = e.TWR_DATA_DE1, TWR_DATA_DE2 = e.TWR_DATA_DE2, TWR_EAN = e.TWR_EAN, TWR_EAN_OPAK = e.TWR_EAN_OPAK, TWR_EAN_PALETY = e.TWR_EAN_PALETY, TWR_ILOSC = e.TWR_ILOSC,TWR_NAZWA=e.TWR_NAZWA,TWR_SSCC=e.TWR_SSCC,TWR_ILOSC_OPAK=e.TWR_ILOSC_OPAK,TWR_ILOSC_SZT=e.TWR_ILOSC_SZT,TWR_KOD_ZBIORCZY=e.TWR_KOD_ZBIORCZY,TWR_NAZWA_DE=e.TWR_NAZWA_DE,TWR_WAGA=e.TWR_WAGA,STATUS=e.STATUS,ZLEC_LINE=e.ZLEC_LINE,ZPL_ID=e.ZPL_ID });


            return ent.SaveChanges() > 0;
        }

        public virtual List<ZZSPE_PRINT> ToPrint()
        {
            Entities en = new Entities();
            List<ZZSPE_PRINT> Toprint = en.ZZSPE_PRINT.Where(e => e.STATUS == 0 && e.ID_PRINTER>0).ToList();
            return Toprint;
        }

        public virtual string PrintData(int gidnumer)
        {
            string datareturn = "";
            Entities en = new Entities();
            List<ZZSPE_DaneWeHist> data = en.ZZSPE_DaneWeHist.Where(e => e.DOK_GIDNumer == gidnumer).ToList();
            foreach( var datas in data)
            {
                datareturn = datas.ROK + "-" + datas.MIESIAC + "-" + datas.DZIEN + " g:" + datas.GODZINA + ":" + datas.MINUTA + ":" + datas.SEKUNDA;
            }

            return datareturn;
        }

        public virtual List<ZZSPE_PRINT> ListPrinter(string code = "")
        {
            var ent = new Entities();
            var dane = ent.ZZSPE_PRINT.ToList();
            List<ZZSPE_PRINT> obj = dane.OrderByDescending(k => k.ID).ToList();

            return obj;
        }

    }
}
