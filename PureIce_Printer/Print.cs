﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PureIce_Printer
{
   
    public class Print
    {
        private static bool Printing { get; set; }
        public void startprinting(bool Printing)
        {
            var logger = NLog.LogManager.GetCurrentClassLogger();
            if (Printing == true)
            {
                logger.Info("START APLIKACJI SSCC_ETYKIETA PRINTER NASLUCHUJE!");

            }
            else
            {
                logger.Info("zamykam");
            }

            while (Printing == true)
            {

                dataload get = new dataload();
                var items = get.ToPrint();
                if (items.Count > 0)
                {
                    foreach (var item in items)
                    {
                        zpl _zpl = new zpl();
                        if (item.ZPL_ID == 1 || item.ZPL_ID==4)
                        {

                            if (item.ID_PRINTER == 1)
                            {
                                _zpl.PrintEtykieciarka(item);
                                get.UpdateStatus(item.ID);
                            }
                           
                            else

                            {
                                if (item.ZPL_ID == 4)
                                {
                                    _zpl.PrintEtykieciarkaZPL4(item);
                                    get.UpdateStatus(item.ID);

                                }
                                else { 
                                // PrintPDF(item);
                                _zpl.PrintEtykieciarkaZapas(item);
                                get.UpdateStatus(item.ID);
                                }

                            }
                        }
                        else
                        {

                            if (item.ZPL_ID == 2 && item.ID_PRINTER == 1)

                            {
                                _zpl.PrintNiemcy(item);
                                get.UpdateStatus(item.ID);
                            }
                            else
                            {
                                if (item.ZPL_ID == 2 && item.ID_PRINTER == 2)
                                {
                                    _zpl.PrintNiemcyZapas(item);
                                    get.UpdateStatus(item.ID);
                                }
                                else
                                {
                                    if (item.ZPL_ID == 3 && item.ID_PRINTER == 2 && item.TWR_WAGA > 2)
                                    {
                                        _zpl.PrintNiemcyZapas(item);
                                        get.UpdateStatus(item.ID);
                                    }
                                    else
                                    {
                                        _zpl.PrintEtykieciarkaZapas(item);
                                        //                    PrintEtykieciarkaZapas(item);
                                        get.UpdateStatus(item.ID);
                                    }

                                }
                            }
                        }

                    }


                }
            }
        }
    }
}
