﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PureIce_Printer
{
 public   class zpl
    {

        public void PrintEtykieciarkaZPL4(ZZSPE_PRINT item)
        {

            var logger = NLog.LogManager.GetCurrentClassLogger();

            string ipAddress_Printer = "192.168.0.109";

            int port = 6101;

            string pierwszy = ">;>802" + item.TWR_EAN_OPAK + "15" + item.TWR_DATA2 + "37" + item.TWR_ILOSC_OPAK;
            string napispodpierwszym =
                "(02)" + item.TWR_EAN_OPAK + "(15)" + item.TWR_DATA2 + "(37)" + item.TWR_ILOSC_OPAK;

            string drugi = ">;>8412590259693800010>6" + item.TWR_CECHA;
            string napispoddrugi = "(412)" + "5902596938000" + "(10)" + item.TWR_CECHA;

            string trzeci = ">;>800" + item.TWR_SSCC;
            string napispodtrzecim = "(00)" + item.TWR_SSCC;
            int przelicznik = 0;
            if (item.TWR_ILOSC_OPAK == 0)
            {
                przelicznik = 1;
            }
            else
            {
                przelicznik = item.TWR_ILOSC_SZT.Value / item.TWR_ILOSC_OPAK.Value;
            }
            if (item.TWR_ILOSC_OPAK == 0)
            {
                item.TWR_ILOSC_OPAK = 60;
            }



            string zbiorczy = ">:" + item.TWR_SSCC + item.TWR_EAN_PALETY + item.TWR_CECHA + 1;
            string zbiorczynapis = item.TWR_SSCC + item.TWR_EAN_PALETY + item.TWR_CECHA + 1;
            string nazwa = "";
            string nowa = "";
            for (int i = 0; i < item.TWR_NAZWA.Length; i++)
            {
                if (item.TWR_NAZWA[i] != 'Ż')
                {
                    nazwa = nazwa + item.TWR_NAZWA[i];
                }
                else
                {
                    nazwa = nazwa + "Z";
                }
            }
            dataload dts = new dataload();
            string dataprint = dts.PrintData(item.DOK_GIDNumer.Value);

            string ZPLString= "^XA"+
"^CF0,60"+
  "^FO100,100 ^FD"+ item.TWR_NAZWA + "^FS"+
      "^BY5,2,270"+
        "^FO100,200 ^BC ^FD"+ item.TWR_KOD_ZBIORCZY.Substring(0, 8) +"^FS"+

       "^BY5,2,270"+
            "^FO100,550 ^BC ^FD" +item.TWR_EAN+ "^FS"+

                "^FO100,900 ^FD"+ dataprint +"^FS" +

                     "^XZ";



            string ZPLString222 = "^XA" +
"^CF0,40" +
 "^FO100,100 ^FD1000"+nazwa+"^FS" +
    "^BY5,2,270" +
     "^FO100,200 ^BC ^FD"+item.TWR_KOD_ZBIORCZY.Substring(0,8)+" ^FS" +
      "^FO100,600 ^FD1000"+item.TWR_DATA+"^FS" +

         "^XZ";




            string ZPLString1 = "^XA ^POI ^CF0,25^CI28^FO1,280^GB1540,1,3^FS " +
                               "^FO1,495^GB1540,1,3^FS " +
                               "^FO409,160^FH^FDp.los@pure-ice.pl^FS " +

                               "^FO160,160^FH^FDtel. 501 341 241^FS " +
                               "^FO140,127^F0^FD05-850 Ożarów Mazowiecki ul Poznańska 165^FS " +
                               "^FO100,70^FH^CF0,35^FDPure Ice Sp. Z.o.o^FS " +

                               "^FO30,220^F0^CF0,40^FD" + nazwa + "^FS" +
                               "^CF0,15" +
                                            "^FO400,220 ^FH ^FD" + item.TWR_ILOSC_OPAK + "x" + przelicznik + "=" +
                               item.TWR_ILOSC_SZT + "  ^FS" +

"^BY2,2,120 ^FT650,100 ^BCN,,N,N" +
                               "^FD" + item.TWR_EAN_OPAK + "^FS" +



                               "^FO50,300^FH^FDCONTENT / ZAWARTOSC ^FS ^" +
                               "^CF0,35" +
                               "^FO50,320^FH^FD" + item.TWR_EAN_OPAK + "^FS ^" +


                               "^CF0,15" +
                               "^FO590,300^FH^FDBATCH/ LOT / SERIA :^FS ^" +
                               "^CF0,35" +
                               "^FO590,320^FH^FD" + item.TWR_CECHA + "^FS ^" +

                               "^CF0,15" +
                               "^FO50,370^FH^FDBEST BEFORE / NAJLEPSZE DO: ^FS ^" +
                               "^CF0,35" +
                               "^FO50,390^FH^FD" + item.TWR_DATA + " (mm.yyyy)^FS ^" +

                               "^CF0,15" +
                               "^FO590,370^FH^FDCOUNT / LICZBA:^FS ^" +
                               "^CF0,35" +
                               "^FO590,390^FH^FD" + item.TWR_ILOSC_OPAK + "^FS ^" +

                               "^CF0,15" +
                               "^FO50,440^FH^FDSSCC: ^FS ^" +
                               "^CF0,35" +
                               "^FO50,460^FH^FD" + item.TWR_SSCC + "^FS ^" +

                               "^CF0,15" +
                               "^FO590,440^FH^FDPURCHASE FROM / KUPIONY OD:^FS" +
                               "^CF0,35" +
                               "^FO590,460^FH^FD5902596938000^FS ^" +



                               "^BY4,3,251 ^FT186,770 ^BCN,,N,N" +
                               "^FD" + pierwszy + "^FS" +
                               "^FT350,800 ^A0N,32,31 ^FH ^FD" + napispodpierwszym + "^FS" +

                               "^BY4,3,251 ^FT100,1070 ^BCN,,N,N" +
                               "^FD" + drugi + "^FS" +
                               "^FT400,1100 ^A0N,32,31 ^FH ^FD" + napispoddrugi + "^FS" +


                               "^BY4,3,251 ^FT230,1370 ^BCN,,N,N" +
                               "^FD" + trzeci + "^FS" +
                               "^FT400,1400 ^A0N,32,31 ^FH ^FD" + napispodtrzecim + "^FS" +
                               "^FO1,1470^GB1540,1,3^FS " +

                               "^BY2,3,160 ^FT50,1640 ^BCN,,N,N" +
                               "^FD" + zbiorczy + "^FS" +
                               "^FT165,1670 ^A0N,32,31 ^FH ^FD" + zbiorczynapis + "^FS" +




                               "^XZ";
            //       string zpltest = "^xa ^CF0,50 ^fo100,100 ^fd Hello World ^fs ^xz";
            byte[] zpl = Encoding.UTF8.GetBytes(ZPLString);
            var request =
                (HttpWebRequest)WebRequest.Create("http://api.labelary.com/v1/printers/8dpmm/labels/5.2x8.2/");
            //  var request = (HttpWebRequest)WebRequest.Create("http://api.labelary.com/v1/printers/8dpmm/labels/3x2/0/");
            request.Method = "POST";
            request.Headers.Add("X-Page-Size", "A5");
            //  request.Headers.Add("X-Page-Layout", "1x2");
            // request.Headers.Add("X-Rotation","90");
            request.Accept = "application/pdf"; // omit this line to get PNG images back
            request.ContentType = "application/x-www-form-urlencoded";
            //    request.ContentType = "X-Page-Layout/2x1";
            //     request.Headers = 'X-Page-Layout 2x1'';
            //    request.ContentType = "/2x1";
            // request.Headers = ;

            request.ContentLength = zpl.Length;

            var requestStream = request.GetRequestStream();
            requestStream.Write(zpl, 0, zpl.Length);
            requestStream.Close();
            DateTime dt = DateTime.Now;
            string data = String.Format("{0:s}", dt);

            string nazwa2 = "ETYKIETA_L" + item.ZLEC_LINE + "_NR_PALETY_" + item.NR_PALETY + "_" + data;

            string poprawionanazwa2 = "";
            for (int i = 0; i < nazwa2.Length; i++)
            {

                if (nazwa2[i] != ':')
                {
                    poprawionanazwa2 = poprawionanazwa2 + nazwa2[i];
                }
                else
                {
                    poprawionanazwa2 = poprawionanazwa2 + "_";
                }
            }

            //  string parth = "";
            string path = @"C:\etykiety\PL\L" + item.ZLEC_LINE + "\\" + poprawionanazwa2;

            //string path2 =path+"]"
            try
            {
                var response = (HttpWebResponse)request.GetResponse();
                var responseStream = response.GetResponseStream();
                var fileStream = File.Create(path + ".pdf"); // change file name for PNG images
                responseStream.CopyTo(fileStream);
                responseStream.Close();
                fileStream.Close();
            }
            catch (WebException e)
            {
                logger.Info("Error: {0}", e.Status);
            }
            ping check = new ping();


            if (check.printer_status(ipAddress_Printer) == true)
            {
                try
                {
                    // Open connection
                    System.Net.Sockets.TcpClient client = new System.Net.Sockets.TcpClient();
                    client.Connect(ipAddress_Printer, port);

                    // Write ZPL String to connection
                    System.IO.StreamWriter writer = new System.IO.StreamWriter(client.GetStream());
                    writer.Write(ZPLString);
                    writer.Flush();

                    logger.Info("Wydrukowane na drukarke 192.168.0.109 - ETYKECIARKA ZAPASOWA");

                    // Close Connection
                    writer.Close();
                    client.Close();
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }


            }

            else
            {
                logger.Info("DRUKARKA ZAPASOWA NIE DOSTEPNA? PING FAILED ");
            }
        }


        public void PrintEtykieciarka(ZZSPE_PRINT item)
        {

            var logger = NLog.LogManager.GetCurrentClassLogger();

            string ipAddress_Printer = "192.168.0.9";

            int port = 9100;

            string pierwszy = ">;>802" + item.TWR_EAN_OPAK + "15" + item.TWR_DATA2 + "37" + item.TWR_ILOSC_OPAK;
            string napispodpierwszym =
                "(02)" + item.TWR_EAN_OPAK + "(15)" + item.TWR_DATA2 + "(37)" + item.TWR_ILOSC_OPAK;

            string drugi = ">;>8412590259693800010>6" + item.TWR_CECHA;
            string napispoddrugi = "(412)" + "5902596938000" + "(10)" + item.TWR_CECHA;

            string trzeci = ">;>800" + item.TWR_SSCC;
            string napispodtrzecim = "(00)" + item.TWR_SSCC;
            int przelicznik = 0;
            if (item.TWR_ILOSC_OPAK == 0)
            {
                przelicznik = 1;
            }
            else
            {
                przelicznik = item.TWR_ILOSC_SZT.Value / item.TWR_ILOSC_OPAK.Value;
            }

            if (item.TWR_ILOSC_OPAK == 0)
            {
                item.TWR_ILOSC_OPAK = 60;
            }


            string zbiorczy = ">:" + item.TWR_SSCC + item.TWR_EAN_PALETY + item.TWR_CECHA + 1;
            string zbiorczynapis = item.TWR_SSCC + item.TWR_EAN_PALETY + item.TWR_CECHA + 1;



            string ZPLString = "^XA ^POI ^CF0,25^CI28^FO1,280^GB1540,1,3^FS " +
                               "^FO1,495^GB1540,1,3^FS " +
                               "^FO409,160^FH^FDp.los@pure-ice.pl^FS " +

                               "^FO160,160^FH^FDtel. 501 341 241^FS " +
                               "^FO140,127^F0^FD05-850 Ożarów Mazowiecki ul Poznańska 165^FS " +
                               "^FO100,70^FH^CF0,35^FDPure Ice Sp. Z.o.o^FS " +

                               "^FO30,220^F0^CF0,40^FD" + item.TWR_NAZWA + "^FS" +
                               "^CF0,15" +
                                                         "^FO400,220 ^FH ^FD" + item.TWR_ILOSC_OPAK + "x" + przelicznik + "=" +
                               item.TWR_ILOSC_SZT + "  ^FS" +

"^BY2,2,120 ^FT650,100 ^BCN,,N,N" +
                               "^FD" + item.TWR_EAN_OPAK + "^FS" +


                               "^FO50,300^FH^FDCONTENT / ZAWARTOSC ^FS ^" +
                               "^CF0,35" +
                               "^FO50,320^FH^FD" + item.TWR_EAN_OPAK + "^FS ^" +


                               "^CF0,15" +
                               "^FO590,300^FH^FDBATCH/ LOT / SERIA :^FS ^" +
                               "^CF0,35" +
                               "^FO590,320^FH^FD" + item.TWR_CECHA + "^FS ^" +

                               "^CF0,15" +
                               "^FO50,370^FH^FDBEST BEFORE / NAJLEPSZE DO: ^FS ^" +
                               "^CF0,35" +
                               "^FO50,390^FH^FD" + item.TWR_DATA + " (mm.yyyy)^FS ^" +

                               "^CF0,15" +
                               "^FO590,370^FH^FDCOUNT / LICZBA:^FS ^" +
                               "^CF0,35" +
                               "^FO590,390^FH^FD" + item.TWR_ILOSC_OPAK + "^FS ^" +

                               "^CF0,15" +
                               "^FO50,440^FH^FDSSCC: ^FS ^" +
                               "^CF0,35" +
                               "^FO50,460^FH^FD" + item.TWR_SSCC + "^FS ^" +

                               "^CF0,15" +
                               "^FO590,440^FH^FDPURCHASE FROM / KUPIONY OD:^FS" +
                               "^CF0,35" +
                               "^FO590,460^FH^FD5902596938000^FS ^" +



                               "^BY4,3,251 ^FT186,770 ^BCN,,N,N" +
                               "^FD" + pierwszy + "^FS" +
                               "^FT350,800 ^A0N,32,31 ^FH ^FD" + napispodpierwszym + "^FS" +

                               "^BY4,3,251 ^FT100,1070 ^BCN,,N,N" +
                               "^FD" + drugi + "^FS" +
                               "^FT400,1100 ^A0N,32,31 ^FH ^FD" + napispoddrugi + "^FS" +


                               "^BY4,3,251 ^FT230,1370 ^BCN,,N,N" +
                               "^FD" + trzeci + "^FS" +
                               "^FT400,1400 ^A0N,32,31 ^FH ^FD" + napispodtrzecim + "^FS" +
                               "^FO1,1470^GB1540,1,3^FS " +

                               "^BY2,3,160 ^FT50,1640 ^BCN,,N,N" +
                               "^FD" + zbiorczy + "^FS" +
                               "^FT165,1670 ^A0N,32,31 ^FH ^FD" + zbiorczynapis + "^FS" +



                               "^XZ";
            string zpltest = "^xa ^CF0,50 ^fo100,100 ^fd Hello World ^fs ^xz";
            byte[] zpl = Encoding.UTF8.GetBytes(ZPLString);
            var request =
                (HttpWebRequest)WebRequest.Create("http://api.labelary.com/v1/printers/8dpmm/labels/5.2x8.2/");
            //  var request = (HttpWebRequest)WebRequest.Create("http://api.labelary.com/v1/printers/8dpmm/labels/3x2/0/");
            request.Method = "POST";
            request.Headers.Add("X-Page-Size", "A5");
            //  request.Headers.Add("X-Page-Layout", "1x2");
            // request.Headers.Add("X-Rotation","90");
            request.Accept = "application/pdf"; // omit this line to get PNG images back
            request.ContentType = "application/x-www-form-urlencoded";
            //    request.ContentType = "X-Page-Layout/2x1";
            //     request.Headers = 'X-Page-Layout 2x1'';
            //    request.ContentType = "/2x1";
            // request.Headers = ;

            request.ContentLength = zpl.Length;

            var requestStream = request.GetRequestStream();
            requestStream.Write(zpl, 0, zpl.Length);
            requestStream.Close();
            DateTime dt = DateTime.Now;
            string data = String.Format("{0:s}", dt);

            string nazwa2 = "ETYKIETA_L" + item.ZLEC_LINE + "_NR_PALETY_" + item.NR_PALETY + "_" + data;

            string poprawionanazwa2 = "";
            for (int i = 0; i < nazwa2.Length; i++)
            {

                if (nazwa2[i] != ':')
                {
                    poprawionanazwa2 = poprawionanazwa2 + nazwa2[i];
                }
                else
                {
                    poprawionanazwa2 = poprawionanazwa2 + "_";
                }
            }

            //  string parth = "";
            string path = @"C:\etykiety\PL\L" + item.ZLEC_LINE + "\\" + poprawionanazwa2;

            //string path2 =path+"]"
            try
            {
                var response = (HttpWebResponse)request.GetResponse();
                var responseStream = response.GetResponseStream();
                var fileStream = File.Create(path + ".pdf"); // change file name for PNG images
                responseStream.CopyTo(fileStream);
                responseStream.Close();
                fileStream.Close();
            }
            catch (WebException e)
            {
                Console.WriteLine("Error: {0}", e.Status);
            }


  ping check = new ping();
            //   check.printer_status(ipAddress_Printer);



            if (check.printer_status(ipAddress_Printer) == true)
            {
                try
                {
                    // Open connection
                    System.Net.Sockets.TcpClient client = new System.Net.Sockets.TcpClient();
                    client.Connect(ipAddress_Printer, port);

                    // Write ZPL String to connection
                    System.IO.StreamWriter writer = new System.IO.StreamWriter(client.GetStream());
                    writer.Write(ZPLString);
                    writer.Flush();

                    logger.Info("Wydrukowane na drukarke 192.168.0.9 - ETYKIETA POLSKA");

                    // Close Connection
                    writer.Close();
                    client.Close();
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }


            }

            else
            {
                logger.Info("DRUKARKA NIE DOSTEPNA? PING FAILED ");
            }
        }


        public void PrintNiemcy(ZZSPE_PRINT item)
        {

            var logger = NLog.LogManager.GetCurrentClassLogger();
            string test = item.TWR_NAZWA;
            string ipAddress_Printer = "192.168.0.9";

            int port = 9100;

            string pierwszy = ">;>802" + item.TWR_EAN_OPAK + "37" + item.TWR_ILOSC_OPAK + "3300000" + item.TWR_WAGA;

            string napispodpierwszym =
                "(02)" + item.TWR_EAN_OPAK + "(37)" + item.TWR_ILOSC_OPAK + "(3300)" + item.TWR_WAGA;

            string drugi = ">;>815" + item.TWR_DATA_DE1 + "10" + item.TWR_DATA_DE2;

            string napispoddrugi = "(15)" + item.TWR_DATA_DE1 + "(10)" + item.TWR_DATA_DE2;

            string trzeci = ">;>800" + item.TWR_SSCC + "9004260042350044";
            string trzecinapis = "(00)" + item.TWR_SSCC + "(90)04260042350044";
            int przelicznik = 0;
            if (item.TWR_ILOSC_OPAK == 0)
            {
                przelicznik = item.TWR_ILOSC_SZT.Value;
            }
            else
            {
                przelicznik = item.TWR_ILOSC_SZT.Value / item.TWR_ILOSC_OPAK.Value;
            }

            string zbiorczy = ">:" + item.TWR_SSCC + item.TWR_EAN_PALETY + item.TWR_CECHA + 1;
            string zbiorczynapis = item.TWR_SSCC + item.TWR_EAN_PALETY + item.TWR_CECHA + 1;
            string sscc1 = "";
            string sscc2 = "";

            for (int i = item.TWR_SSCC.Length - 1; i > 16; i--)
            {
                sscc1 = item.TWR_SSCC[i] + sscc1;
            }


            for (int i = item.TWR_SSCC.Length - 2; i > 11; i--)
            {
                sscc2 = item.TWR_SSCC[i] + sscc2;
            }


            string ZPLString = "^XA  ^POI ^CF0,25^CI28^FO1,215^GB1540,1,3^FS " +

                            " ^FO170,180 ^CF0,30 ^FH ^FD63477 Maintal ^FS" +
            " ^FO170,145 ^CF0,30 ^FH ^FDEdisonstraße 7 ^FS" +
                "^FO170,105 ^CF0,30 ^F0 ^FDIce Age Ice GmbH ^FS" +
                " ^FO170,70 ^FH ^CF0,20 ^FDAbsender:^FS" +
                "^FO170,225 ^CF0,55 ^F0 ^FDEiswűrfel Karton 6 x 2 kg^FS" +
                "^FO1,270 ^GB1540,1,3 ^FS" +
                "^FO170,285 ^CF0,30 ^F0 ^FDNVE:^FS" +
                "^FO295,285 ^CF0,45 ^F0 ^FD3 426004235 00" + sscc2 + " " + sscc1 + "^FS" +
                "^FO1,330 ^GB1540,1,3 ^FS" +
                "^FO170,345 ^CF0,20 ^F0 ^FDENTH.EAN:^FS" +
                "^FO170,365 ^CF0,35 ^F0 ^FD" + item.TWR_EAN_OPAK + "^FS" +
                "^FO660,345 ^CF0,20 ^F0 ^FDMHD(TT.MM.JJJJ):     ^FS" +
                " ^FO660,365 ^CF0,35 ^F0 ^FD" + item.TWR_DATA_DE + "^FS" +
                "^FO1,405 ^GB1540,1,3 ^FS" +

                "^FO170,420 ^CF0,20 ^F0 ^FDMENGE:     ^FS" +
                "^FO170,440 ^CF0,35 ^F0 ^FD" + item.TWR_ILOSC_OPAK + " ^FS" +
                "^FO660,420 ^CF0,20 ^F0 ^FDCHARGE:          ^FS" +
                "^FO660,440 ^CF0,35 ^F0 ^FD" + item.TWR_DATA_DE2 + "^FS" +
                "^FO1,480 ^GB1540,1,3 ^FS" +

                "   ^FO170,495 ^CF0,20 ^F0 ^FDEAN der Konsumenteneinh.:         ^FS" +
                "  ^FO170,515 ^CF0,35 ^F0 ^FD426004235 004 4 ^FS" +
                " ^FO660,495 ^CF0,20 ^F0 ^FDBRUT.GEW. (Kg):       ^FS" +
                "^FO660,515 ^CF0,35 ^F0 ^FD" + item.TWR_WAGA + " ^FS" +
                "^FO1,555 ^GB1540,1,3 ^FS" +

                "^BY4,3,200 ^FT170,780 ^BCN,,N,N" +
                "^FD" + pierwszy + "^FS" +
                "^FT345,815 ^A0N,32,31 ^FH ^FD" + napispodpierwszym + "^FS" +

              "                ^BY4,3,200 ^FT275,1060 ^BCN,,N,N" +
               "^FD" + drugi + "^FS" +
                "^FT434,1095 ^A0N,32,31 ^FH ^FD" + napispoddrugi + "^FS" +


              "^FO120,1150 ^CF0,55 ^F0 ^FDN ^FS" +
                "^FO120,1225 ^CF0,55 ^F0 ^FDV ^FS" +
              "^FO120,1300 ^CF0,55 ^F0 ^FDE ^FS" +

              "^BY3,3,200 ^FT170,1340 ^BCN,,N,N" +
               "^FD" + trzeci + " ^FS" +
                  "^FT285,1375 ^A0N,32,31 ^FH ^FD" + trzecinapis + "^FS" +
               "^BY4,3,190 ^FT80,1580 ^BCN,,Y,N" +
                           //    "^FD"+zbiorczy+" ^FS" +
                           //   "^BY2,3,160 ^FT50,1640 ^BCN,,N,N" +
                           // "^FD" + zbiorczy + "^FS" +
                           //"//^FT165,1670 ^A0N,32,31 ^FH ^FD" + zbiorczynapis + "^FS" +

                           //        "^BY2,3,62 ^FT25,1400 ^BCN,,Y,N"+
                           //       "^FD >; 342600423500100028201000000105 > 69L 15 051 ^FS"+
                           //      "^BY1,3,80 ^FT105,1400 ^BCN,,Y,N"+
                           //    "     ^FD >; 342600423500100028201000000105 > 69L 15 051 ^FS"+
                           //  " ^BY2,3,63 ^FT0,1600 ^BCN,,Y,N"+
                           //            "^FD >; 342600423500100028201000000105 > 69L 15 051 ^FS"+
                           "^FO1,1450^GB1540,1,3^FS " +

                           "^BY2,3,160 ^FT120,1620 ^BCN,,N,N" +
                           "^FD" + zbiorczy + "^FS" +
                            "^FT235,1650 ^A0N,32,31 ^FH ^FD" + zbiorczynapis + "^FS" +
                "^XZ";










            byte[] zpl = Encoding.UTF8.GetBytes(ZPLString);
            var request =
                (HttpWebRequest)WebRequest.Create("http://api.labelary.com/v1/printers/8dpmm/labels/5.2x8.2/");
            //  var request = (HttpWebRequest)WebRequest.Create("http://api.labelary.com/v1/printers/8dpmm/labels/3x2/0/");
            request.Method = "POST";
            request.Headers.Add("X-Page-Size", "A5");
            //  request.Headers.Add("X-Page-Layout", "1x2");
            // request.Headers.Add("X-Rotation","90");
            request.Accept = "application/pdf"; // omit this line to get PNG images back
            request.ContentType = "application/x-www-form-urlencoded";
            //    request.ContentType = "X-Page-Layout/2x1";
            //     request.Headers = 'X-Page-Layout 2x1'';
            //    request.ContentType = "/2x1";
            // request.Headers = ;

            request.ContentLength = zpl.Length;

            var requestStream = request.GetRequestStream();
            requestStream.Write(zpl, 0, zpl.Length);
            requestStream.Close();
            DateTime dt = DateTime.Now;
            string data = String.Format("{0:s}", dt);

            string nazwa2 = "ETYKIETA_L" + item.ZLEC_LINE + "_NR_PALETY_" + item.NR_PALETY + "_" + data;

            string poprawionanazwa2 = "";
            for (int i = 0; i < nazwa2.Length; i++)
            {

                if (nazwa2[i] != ':')
                {
                    poprawionanazwa2 = poprawionanazwa2 + nazwa2[i];
                }
                else
                {
                    poprawionanazwa2 = poprawionanazwa2 + "_";
                }
            }

            //  string parth = "";
            string path = @"C:\etykiety\DE\L" + item.ZLEC_LINE + "\\" + poprawionanazwa2;

            //string path2 =path+"]"
            try
            {
                var response = (HttpWebResponse)request.GetResponse();
                var responseStream = response.GetResponseStream();
                var fileStream = File.Create(path + ".pdf"); // change file name for PNG images
                responseStream.CopyTo(fileStream);
                responseStream.Close();
                fileStream.Close();
            }
            catch (WebException e)
            {
                Console.WriteLine("Error: {0}", e.Status);
            }


   ping check = new ping();
            //  nn.check(ipAddress_Printer);



            if (check.printer_status(ipAddress_Printer) == true)
            {
                try
                {
                    // Open connection
                    System.Net.Sockets.TcpClient client = new System.Net.Sockets.TcpClient();
                    client.Connect(ipAddress_Printer, port);

                    // Write ZPL String to connection
                    System.IO.StreamWriter writer = new System.IO.StreamWriter(client.GetStream());
                    writer.Write(ZPLString);
                    writer.Flush();

                    logger.Info("Wydrukowane na drukarke 192.168.0.9 - ETYKIETA NIEMCY");

                    // Close Connection
                    writer.Close();
                    client.Close();
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }


            }

            else
            {
                logger.Info("DRUKARKA APLIKATORA NIE DOSTEPNA? PING FAILED ");
            }
        }


        public void PrintNiemcyZapas(ZZSPE_PRINT item)
        {

            var logger = NLog.LogManager.GetCurrentClassLogger();
            string test = item.TWR_NAZWA;
            string ipAddress_Printer = "192.168.0.109";

            int port = 6101;

            string pierwszy = ">;>802" + item.TWR_EAN_OPAK + "37" + item.TWR_ILOSC_OPAK + "3300000" + item.TWR_WAGA;

            string napispodpierwszym =
                "(02)" + item.TWR_EAN_OPAK + "(37)" + item.TWR_ILOSC_OPAK + "(3300)" + item.TWR_WAGA;

            string drugi = ">;>815" + item.TWR_DATA_DE1 + "10" + item.TWR_DATA_DE2;

            string napispoddrugi = "(15)" + item.TWR_DATA_DE1 + "(10)" + item.TWR_DATA_DE2;

            string trzeci = ">;>800" + item.TWR_SSCC + "9004260042350044";
            string trzecinapis = "(00)" + item.TWR_SSCC + "(90)04260042350044";
            int przelicznik = 0;
            if (item.TWR_ILOSC_OPAK == 0)
            {
                przelicznik = item.TWR_ILOSC_SZT.Value;
            }
            else
            {
                przelicznik = item.TWR_ILOSC_SZT.Value / item.TWR_ILOSC_OPAK.Value;
            }

            string zbiorczy = ">:" + item.TWR_SSCC + item.TWR_EAN_PALETY + item.TWR_CECHA + 1;
            string zbiorczynapis = item.TWR_SSCC + item.TWR_EAN_PALETY + item.TWR_CECHA + 1;
            string sscc1 = "";
            string sscc2 = "";

            for (int i = item.TWR_SSCC.Length - 1; i > 16; i--)
            {
                sscc1 = item.TWR_SSCC[i] + sscc1;
            }


            for (int i = item.TWR_SSCC.Length - 2; i > 11; i--)
            {
                sscc2 = item.TWR_SSCC[i] + sscc2;
            }


            string ZPLString = "^XA  ^POI ^CF0,25^CI28^FO1,215^GB1540,1,3^FS " +

                            " ^FO170,180 ^CF0,30 ^FH ^FD63477 Maintal ^FS" +
            " ^FO170,145 ^CF0,30 ^FH ^FDEdisonstraße 7 ^FS" +
                "^FO170,105 ^CF0,30 ^F0 ^FDIce Age Ice GmbH ^FS" +
                " ^FO170,70 ^FH ^CF0,20 ^FDAbsender:^FS" +
                "^FO170,225 ^CF0,55 ^F0 ^FDEiswűrfel Karton 6 x 2 kg^FS" +
                "^FO1,270 ^GB1540,1,3 ^FS" +
                "^FO170,285 ^CF0,30 ^F0 ^FDNVE:^FS" +
                "^FO295,285 ^CF0,45 ^F0 ^FD3 426004235 00" + sscc2 + " " + sscc1 + "^FS" +
                "^FO1,330 ^GB1540,1,3 ^FS" +
                "^FO170,345 ^CF0,20 ^F0 ^FDENTH.EAN:^FS" +
                "^FO170,365 ^CF0,35 ^F0 ^FD" + item.TWR_EAN_OPAK + "^FS" +
                "^FO660,345 ^CF0,20 ^F0 ^FDMHD(TT.MM.JJJJ):     ^FS" +
                " ^FO660,365 ^CF0,35 ^F0 ^FD" + item.TWR_DATA_DE + "^FS" +
                "^FO1,405 ^GB1540,1,3 ^FS" +

                "^FO170,420 ^CF0,20 ^F0 ^FDMENGE:     ^FS" +
                "^FO170,440 ^CF0,35 ^F0 ^FD" + item.TWR_ILOSC_OPAK + " ^FS" +
                "^FO660,420 ^CF0,20 ^F0 ^FDCHARGE:          ^FS" +
                "^FO660,440 ^CF0,35 ^F0 ^FD" + item.TWR_DATA_DE2 + "^FS" +
                "^FO1,480 ^GB1540,1,3 ^FS" +

                "   ^FO170,495 ^CF0,20 ^F0 ^FDEAN der Konsumenteneinh.:         ^FS" +
                "  ^FO170,515 ^CF0,35 ^F0 ^FD426004235 004 4 ^FS" +
                " ^FO660,495 ^CF0,20 ^F0 ^FDBRUT.GEW. (Kg):       ^FS" +
                "^FO660,515 ^CF0,35 ^F0 ^FD" + item.TWR_WAGA + " ^FS" +
                "^FO1,555 ^GB1540,1,3 ^FS" +

                "^BY4,3,200 ^FT170,780 ^BCN,,N,N" +
                "^FD" + pierwszy + "^FS" +
                "^FT345,815 ^A0N,32,31 ^FH ^FD" + napispodpierwszym + "^FS" +

              "                ^BY4,3,200 ^FT275,1060 ^BCN,,N,N" +
               "^FD" + drugi + "^FS" +
                "^FT434,1095 ^A0N,32,31 ^FH ^FD" + napispoddrugi + "^FS" +


              "^FO120,1150 ^CF0,55 ^F0 ^FDN ^FS" +
                "^FO120,1225 ^CF0,55 ^F0 ^FDV ^FS" +
              "^FO120,1300 ^CF0,55 ^F0 ^FDE ^FS" +

              "^BY3,3,200 ^FT170,1340 ^BCN,,N,N" +
               "^FD" + trzeci + " ^FS" +
                  "^FT285,1375 ^A0N,32,31 ^FH ^FD" + trzecinapis + "^FS" +
               "^BY4,3,190 ^FT80,1580 ^BCN,,Y,N" +
                           //    "^FD"+zbiorczy+" ^FS" +
                           //   "^BY2,3,160 ^FT50,1640 ^BCN,,N,N" +
                           // "^FD" + zbiorczy + "^FS" +
                           //"//^FT165,1670 ^A0N,32,31 ^FH ^FD" + zbiorczynapis + "^FS" +

                           //        "^BY2,3,62 ^FT25,1400 ^BCN,,Y,N"+
                           //       "^FD >; 342600423500100028201000000105 > 69L 15 051 ^FS"+
                           //      "^BY1,3,80 ^FT105,1400 ^BCN,,Y,N"+
                           //    "     ^FD >; 342600423500100028201000000105 > 69L 15 051 ^FS"+
                           //  " ^BY2,3,63 ^FT0,1600 ^BCN,,Y,N"+
                           //            "^FD >; 342600423500100028201000000105 > 69L 15 051 ^FS"+
                           "^FO1,1450^GB1540,1,3^FS " +

                           "^BY2,3,160 ^FT120,1620 ^BCN,,N,N" +
                           "^FD" + zbiorczy + "^FS" +
                            "^FT235,1650 ^A0N,32,31 ^FH ^FD" + zbiorczynapis + "^FS" +
                "^XZ";










            byte[] zpl = Encoding.UTF8.GetBytes(ZPLString);
            var request =
                (HttpWebRequest)WebRequest.Create("http://api.labelary.com/v1/printers/8dpmm/labels/5.2x8.2/");
            //  var request = (HttpWebRequest)WebRequest.Create("http://api.labelary.com/v1/printers/8dpmm/labels/3x2/0/");
            request.Method = "POST";
            request.Headers.Add("X-Page-Size", "A5");
            //  request.Headers.Add("X-Page-Layout", "1x2");
            // request.Headers.Add("X-Rotation","90");
            request.Accept = "application/pdf"; // omit this line to get PNG images back
            request.ContentType = "application/x-www-form-urlencoded";
            //    request.ContentType = "X-Page-Layout/2x1";
            //     request.Headers = 'X-Page-Layout 2x1'';
            //    request.ContentType = "/2x1";
            // request.Headers = ;

            request.ContentLength = zpl.Length;

            var requestStream = request.GetRequestStream();
            requestStream.Write(zpl, 0, zpl.Length);
            requestStream.Close();
            DateTime dt = DateTime.Now;
            string data = String.Format("{0:s}", dt);

            string nazwa2 = "ETYKIETA_L" + item.ZLEC_LINE + "_NR_PALETY_" + item.NR_PALETY + "_" + data;

            string poprawionanazwa2 = "";
            for (int i = 0; i < nazwa2.Length; i++)
            {

                if (nazwa2[i] != ':')
                {
                    poprawionanazwa2 = poprawionanazwa2 + nazwa2[i];
                }
                else
                {
                    poprawionanazwa2 = poprawionanazwa2 + "_";
                }
            }

            //  string parth = "";
            string path = @"C:\etykiety\DE\L" + item.ZLEC_LINE + "\\" + poprawionanazwa2;

            //string path2 =path+"]"
            try
            {
                var response = (HttpWebResponse)request.GetResponse();
                var responseStream = response.GetResponseStream();
                var fileStream = File.Create(path + ".pdf"); // change file name for PNG images
                responseStream.CopyTo(fileStream);
                responseStream.Close();
                fileStream.Close();
            }
            catch (WebException e)
            {
                Console.WriteLine("Error: {0}", e.Status);
            }


            ping check = new ping();
         //   nn.check(ipAddress_Printer);



            if (check.printer_status(ipAddress_Printer) == true)
            {
                try
                {
                    // Open connection
                    System.Net.Sockets.TcpClient client = new System.Net.Sockets.TcpClient();
                    client.Connect(ipAddress_Printer, port);

                    // Write ZPL String to connection
                    System.IO.StreamWriter writer = new System.IO.StreamWriter(client.GetStream());
                    writer.Write(ZPLString);
                    writer.Flush();

                    logger.Info("Wydrukowane na drukarke 192.168.0.109- ZAPASOWA ETYKIECIARKA - ETYKIETA NIEMCY");

                    // Close Connection
                    writer.Close();
                    client.Close();
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }


            }

            else
            {
                logger.Info("DRUKARKA ZAPASOWA NIE DOSTEPNA? PING FAILED ");
            }
        }


        public void PrintEtykieciarkaZapas(ZZSPE_PRINT item)
        {

            var logger = NLog.LogManager.GetCurrentClassLogger();

            string ipAddress_Printer = "192.168.0.109";

            int port = 6101;

            string pierwszy = ">;>802" + item.TWR_EAN_OPAK + "15" + item.TWR_DATA2 + "37" + item.TWR_ILOSC_OPAK;
            string napispodpierwszym =
                "(02)" + item.TWR_EAN_OPAK + "(15)" + item.TWR_DATA2 + "(37)" + item.TWR_ILOSC_OPAK;

            string drugi = ">;>8412590259693800010>6" + item.TWR_CECHA;
            string napispoddrugi = "(412)" + "5902596938000" + "(10)" + item.TWR_CECHA;

            string trzeci = ">;>800" + item.TWR_SSCC;
            string napispodtrzecim = "(00)" + item.TWR_SSCC;
            int przelicznik = 0;
            if (item.TWR_ILOSC_OPAK == 0)
            {
                przelicznik = 1;
            }
            else
            {
                przelicznik = item.TWR_ILOSC_SZT.Value / item.TWR_ILOSC_OPAK.Value;
            }
            if (item.TWR_ILOSC_OPAK == 0)
            {
                item.TWR_ILOSC_OPAK = 60;
            }



            string zbiorczy = ">:" + item.TWR_SSCC + item.TWR_EAN_PALETY + item.TWR_CECHA + 1;
            string zbiorczynapis = item.TWR_SSCC + item.TWR_EAN_PALETY + item.TWR_CECHA + 1;
            string nazwa = "";
            string nowa = "";
            for (int i = 0; i < item.TWR_NAZWA.Length; i++)
            {
                if (item.TWR_NAZWA[i] != 'Ż')
                {
                    nazwa = nazwa + item.TWR_NAZWA[i];
                }
                else
                {
                    nazwa = nazwa + "Z";
                }
            }




            string ZPLString = "^XA ^POI ^CF0,25^CI28^FO1,280^GB1540,1,3^FS " +
                               "^FO1,495^GB1540,1,3^FS " +
                               "^FO409,160^FH^FDp.los@pure-ice.pl^FS " +

                               "^FO160,160^FH^FDtel. 501 341 241^FS " +
                               "^FO140,127^F0^FD05-850 Ożarów Mazowiecki ul Poznańska 165^FS " +
                               "^FO100,70^FH^CF0,35^FDPure Ice Sp. Z.o.o^FS " +

                               "^FO30,220^F0^CF0,40^FD" + nazwa + "^FS" +
                               "^CF0,15" +
                                            "^FO400,220 ^FH ^FD" + item.TWR_ILOSC_OPAK + "x" + przelicznik + "=" +
                               item.TWR_ILOSC_SZT + "  ^FS" +

"^BY2,2,120 ^FT650,100 ^BCN,,N,N" +
                               "^FD" + item.TWR_EAN_OPAK + "^FS" +



                               "^FO50,300^FH^FDCONTENT / ZAWARTOSC ^FS ^" +
                               "^CF0,35" +
                               "^FO50,320^FH^FD" + item.TWR_EAN_OPAK + "^FS ^" +


                               "^CF0,15" +
                               "^FO590,300^FH^FDBATCH/ LOT / SERIA :^FS ^" +
                               "^CF0,35" +
                               "^FO590,320^FH^FD" + item.TWR_CECHA + "^FS ^" +

                               "^CF0,15" +
                               "^FO50,370^FH^FDBEST BEFORE / NAJLEPSZE DO: ^FS ^" +
                               "^CF0,35" +
                               "^FO50,390^FH^FD" + item.TWR_DATA + " (mm.yyyy)^FS ^" +

                               "^CF0,15" +
                               "^FO590,370^FH^FDCOUNT / LICZBA:^FS ^" +
                               "^CF0,35" +
                               "^FO590,390^FH^FD" + item.TWR_ILOSC_OPAK + "^FS ^" +

                               "^CF0,15" +
                               "^FO50,440^FH^FDSSCC: ^FS ^" +
                               "^CF0,35" +
                               "^FO50,460^FH^FD" + item.TWR_SSCC + "^FS ^" +

                               "^CF0,15" +
                               "^FO590,440^FH^FDPURCHASE FROM / KUPIONY OD:^FS" +
                               "^CF0,35" +
                               "^FO590,460^FH^FD5902596938000^FS ^" +



                               "^BY4,3,251 ^FT186,770 ^BCN,,N,N" +
                               "^FD" + pierwszy + "^FS" +
                               "^FT350,800 ^A0N,32,31 ^FH ^FD" + napispodpierwszym + "^FS" +

                               "^BY4,3,251 ^FT100,1070 ^BCN,,N,N" +
                               "^FD" + drugi + "^FS" +
                               "^FT400,1100 ^A0N,32,31 ^FH ^FD" + napispoddrugi + "^FS" +


                               "^BY4,3,251 ^FT230,1370 ^BCN,,N,N" +
                               "^FD" + trzeci + "^FS" +
                               "^FT400,1400 ^A0N,32,31 ^FH ^FD" + napispodtrzecim + "^FS" +
                               "^FO1,1470^GB1540,1,3^FS " +

                               "^BY2,3,160 ^FT50,1640 ^BCN,,N,N" +
                               "^FD" + zbiorczy + "^FS" +
                               "^FT165,1670 ^A0N,32,31 ^FH ^FD" + zbiorczynapis + "^FS" +




                               "^XZ";
            //       string zpltest = "^xa ^CF0,50 ^fo100,100 ^fd Hello World ^fs ^xz";
            byte[] zpl = Encoding.UTF8.GetBytes(ZPLString);
            var request =
                (HttpWebRequest)WebRequest.Create("http://api.labelary.com/v1/printers/8dpmm/labels/5.2x8.2/");
            //  var request = (HttpWebRequest)WebRequest.Create("http://api.labelary.com/v1/printers/8dpmm/labels/3x2/0/");
            request.Method = "POST";
            request.Headers.Add("X-Page-Size", "A5");
            //  request.Headers.Add("X-Page-Layout", "1x2");
            // request.Headers.Add("X-Rotation","90");
            request.Accept = "application/pdf"; // omit this line to get PNG images back
            request.ContentType = "application/x-www-form-urlencoded";
            //    request.ContentType = "X-Page-Layout/2x1";
            //     request.Headers = 'X-Page-Layout 2x1'';
            //    request.ContentType = "/2x1";
            // request.Headers = ;

            request.ContentLength = zpl.Length;

            var requestStream = request.GetRequestStream();
            requestStream.Write(zpl, 0, zpl.Length);
            requestStream.Close();
            DateTime dt = DateTime.Now;
            string data = String.Format("{0:s}", dt);

            string nazwa2 = "ETYKIETA_L" + item.ZLEC_LINE + "_NR_PALETY_" + item.NR_PALETY + "_" + data;

            string poprawionanazwa2 = "";
            for (int i = 0; i < nazwa2.Length; i++)
            {

                if (nazwa2[i] != ':')
                {
                    poprawionanazwa2 = poprawionanazwa2 + nazwa2[i];
                }
                else
                {
                    poprawionanazwa2 = poprawionanazwa2 + "_";
                }
            }

            //  string parth = "";
            string path = @"C:\etykiety\PL\L" + item.ZLEC_LINE + "\\" + poprawionanazwa2;

            //string path2 =path+"]"
            try
            {
                var response = (HttpWebResponse)request.GetResponse();
                var responseStream = response.GetResponseStream();
                var fileStream = File.Create(path + ".pdf"); // change file name for PNG images
                responseStream.CopyTo(fileStream);
                responseStream.Close();
                fileStream.Close();
            }
            catch (WebException e)
            {
                logger.Info("Error: {0}", e.Status);
            }
          ping check = new ping();


            if (check.printer_status(ipAddress_Printer) == true)
            {
                try
                {
                    // Open connection
                    System.Net.Sockets.TcpClient client = new System.Net.Sockets.TcpClient();
                    client.Connect(ipAddress_Printer, port);

                    // Write ZPL String to connection
                    System.IO.StreamWriter writer = new System.IO.StreamWriter(client.GetStream());
                    writer.Write(ZPLString);
                    writer.Flush();

                    logger.Info("Wydrukowane na drukarke 192.168.0.109 - ETYKECIARKA ZAPASOWA");

                    // Close Connection
                    writer.Close();
                    client.Close();
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }


            }

            else
            {
                logger.Info("DRUKARKA ZAPASOWA NIE DOSTEPNA? PING FAILED ");
            }
        }


        public void PrintEtykieciarkaZapasWydrukSeryjny(ZZSPE_PRINT item)
        {

            var logger = NLog.LogManager.GetCurrentClassLogger();

            string ipAddress_Printer = "192.168.0.109";

            int port = 6101;

            string pierwszy = ">;>802" + item.TWR_EAN_OPAK + "15" + item.TWR_DATA2 + "37" + item.TWR_ILOSC_OPAK;
            string napispodpierwszym =
                "(02)" + item.TWR_EAN_OPAK + "(15)" + item.TWR_DATA2 + "(37)" + item.TWR_ILOSC_OPAK;

            string drugi = ">;>8412590259693800010>6" + item.TWR_CECHA;
            string napispoddrugi = "(412)" + "5902596938000" + "(10)" + item.TWR_CECHA;

            string trzeci = ">;>800" + item.TWR_SSCC;
            string napispodtrzecim = "(00)" + item.TWR_SSCC;
            int przelicznik = 0;
            if (item.TWR_ILOSC_OPAK == 0)
            {
                przelicznik = 1;
            }
            else
            {
                przelicznik = item.TWR_ILOSC_SZT.Value / item.TWR_ILOSC_OPAK.Value;
            }
            if (item.TWR_ILOSC_OPAK == 0)
            {
                item.TWR_ILOSC_OPAK = 60;
            }

            string zbiorczy = ">:" + item.TWR_SSCC + item.TWR_EAN_PALETY + item.TWR_CECHA + 1;
            string zbiorczynapis = item.TWR_SSCC + item.TWR_EAN_PALETY + item.TWR_CECHA + 1;
            string nazwa = "";
            string nowa = "";
            for (int i = 0; i < item.TWR_NAZWA.Length; i++)
            {
                if (item.TWR_NAZWA[i] != 'Ż')
                {
                    nazwa = nazwa + item.TWR_NAZWA[i];
                }
                else
                {
                    nazwa = nazwa + "Z";
                }
            }




            string ZPLString = "^XA ^POI ^CF0,25^CI28^FO1,280^GB1540,1,3^FS " +
                               "^FO1,495^GB1540,1,3^FS " +
                               "^FO409,160^FH^FDp.los@pure-ice.pl^FS " +

                               "^FO160,160^FH^FDtel. 501 341 241^FS " +
                               "^FO140,127^F0^FD05-850 Ożarów Mazowiecki ul Poznańska 165^FS " +
                               "^FO100,70^FH^CF0,35^FDPure Ice Sp. Z.o.o^FS " +

                               "^FO30,220^F0^CF0,40^FD" + nazwa + "^FS" +
                               "^CF0,15" +
                                       "^FO400,220 ^FH ^FD" + item.TWR_ILOSC_OPAK + "x" + przelicznik + "=" +
                               item.TWR_ILOSC_SZT + "  ^FS" +

"^BY2,2,120 ^FT650,100 ^BCN,,N,N" +
                               "^FD" + item.TWR_EAN_OPAK + "^FS" +


                               "^FO50,300^FH^FDCONTENT / ZAWARTOSC ^FS ^" +
                               "^CF0,35" +
                               "^FO50,320^FH^FD" + item.TWR_EAN_OPAK + "^FS ^" +


                               "^CF0,15" +
                               "^FO590,300^FH^FDBATCH/ LOT / SERIA :^FS ^" +
                               "^CF0,35" +
                               "^FO590,320^FH^FD" + item.TWR_CECHA + "^FS ^" +

                               "^CF0,15" +
                               "^FO50,370^FH^FDBEST BEFORE / NAJLEPSZE DO: ^FS ^" +
                               "^CF0,35" +
                               "^FO50,390^FH^FD" + item.TWR_DATA + " (mm.yyyy)^FS ^" +

                               "^CF0,15" +
                               "^FO590,370^FH^FDCOUNT / LICZBA:^FS ^" +
                               "^CF0,35" +
                               "^FO590,390^FH^FD" + item.TWR_ILOSC_OPAK + "^FS ^" +

                               "^CF0,15" +
                               "^FO50,440^FH^FDSSCC: ^FS ^" +
                               "^CF0,35" +
                               "^FO50,460^FH^FD" + item.TWR_SSCC + "^FS ^" +

                               "^CF0,15" +
                               "^FO590,440^FH^FDPURCHASE FROM / KUPIONY OD:^FS" +
                               "^CF0,35" +
                               "^FO590,460^FH^FD5902596938000^FS ^" +



                               "^BY4,3,251 ^FT186,770 ^BCN,,N,N" +
                               "^FD" + pierwszy + "^FS" +
                               "^FT350,800 ^A0N,32,31 ^FH ^FD" + napispodpierwszym + "^FS" +

                               "^BY4,3,251 ^FT100,1070 ^BCN,,N,N" +
                               "^FD" + drugi + "^FS" +
                               "^FT400,1100 ^A0N,32,31 ^FH ^FD" + napispoddrugi + "^FS" +


                               "^BY4,3,251 ^FT230,1370 ^BCN,,N,N" +
                               "^FD" + trzeci + "^FS" +
                               "^FT400,1400 ^A0N,32,31 ^FH ^FD" + napispodtrzecim + "^FS" +
                               "^FO1,1470^GB1540,1,3^FS " +

                               "^BY2,3,160 ^FT50,1640 ^BCN,,N,N" +
                               "^FD" + zbiorczy + "^FS" +
                               "^FT165,1670 ^A0N,32,31 ^FH ^FD" + zbiorczynapis + "^FS" +
                               "^PQ2" +



                               "^XZ";
            //       string zpltest = "^xa ^CF0,50 ^fo100,100 ^fd Hello World ^fs ^xz";
            byte[] zpl = Encoding.UTF8.GetBytes(ZPLString);
            var request =
                (HttpWebRequest)WebRequest.Create("http://api.labelary.com/v1/printers/8dpmm/labels/5.2x8.2/");
            //  var request = (HttpWebRequest)WebRequest.Create("http://api.labelary.com/v1/printers/8dpmm/labels/3x2/0/");
            request.Method = "POST";
            request.Headers.Add("X-Page-Size", "A5");
            //  request.Headers.Add("X-Page-Layout", "1x2");
            // request.Headers.Add("X-Rotation","90");
            request.Accept = "application/pdf"; // omit this line to get PNG images back
            request.ContentType = "application/x-www-form-urlencoded";
            //    request.ContentType = "X-Page-Layout/2x1";
            //     request.Headers = 'X-Page-Layout 2x1'';
            //    request.ContentType = "/2x1";
            // request.Headers = ;

            request.ContentLength = zpl.Length;

            var requestStream = request.GetRequestStream();
            requestStream.Write(zpl, 0, zpl.Length);
            requestStream.Close();
            DateTime dt = DateTime.Now;
            string data = String.Format("{0:s}", dt);

            string nazwa2 = "ETYKIETA_L" + item.ZLEC_LINE + "_NR_PALETY_" + item.NR_PALETY + "_" + data;

            string poprawionanazwa2 = "";
            for (int i = 0; i < nazwa2.Length; i++)
            {

                if (nazwa2[i] != ':')
                {
                    poprawionanazwa2 = poprawionanazwa2 + nazwa2[i];
                }
                else
                {
                    poprawionanazwa2 = poprawionanazwa2 + "_";
                }
            }

            //  string parth = "";
            string path = @"C:\etykiety\L" + item.ZLEC_LINE + "\\" + poprawionanazwa2;

            //string path2 =path+"]"
            try
            {
                var response = (HttpWebResponse)request.GetResponse();
                var responseStream = response.GetResponseStream();
                var fileStream = File.Create(path + ".pdf"); // change file name for PNG images
                responseStream.CopyTo(fileStream);
                responseStream.Close();
                fileStream.Close();
            }
            catch (WebException e)
            {
                Console.WriteLine("Error: {0}", e.Status);
            }
           ping check = new ping();
            //    nnn.check(ipAddress_Printer);
            //   ping nnn = new pingzas();
          //ping nn = new ping();
            //nn.check();



            if (check.printer_status(ipAddress_Printer) == true)
            {
                try
                {
                    // Open connection
                    System.Net.Sockets.TcpClient client = new System.Net.Sockets.TcpClient();
                    client.Connect(ipAddress_Printer, port);

                    // Write ZPL String to connection
                    System.IO.StreamWriter writer = new System.IO.StreamWriter(client.GetStream());
                    writer.Write(ZPLString);
                    writer.Flush();

                    logger.Info("Wydrukowane na drukarke 192.168.0.109 - DRUKARKA APLIKATORA");

                    // Close Connection
                    writer.Close();
                    client.Close();
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }


            }

            else
            {
                logger.Info("DRUKARKA APLIKATORA NIE DOSTEPNA? PING FAILED ");
            }
        }


        public void PrintPDF(ZZSPE_PRINT item)
        {

            var logger = NLog.LogManager.GetCurrentClassLogger();

            string pierwszy = ">;>802" + item.TWR_EAN_OPAK + "15" + item.TWR_DATA2 + "37" + item.TWR_ILOSC_OPAK;
            string napispodpierwszym =
                "(02)" + item.TWR_EAN_OPAK + "(15)" + item.TWR_DATA2 + "(37)" + item.TWR_ILOSC_OPAK;

            string drugi = ">;>8412590259693800010>6" + item.TWR_CECHA;
            string napispoddrugi = "(412)" + "5902596938000" + "(10)" + item.TWR_CECHA;

            string trzeci = ">;>800" + item.TWR_SSCC;
            string napispodtrzecim = "(00)" + item.TWR_SSCC;
            int przelicznik;
            if (item.TWR_ILOSC_OPAK == 0)
            {
                przelicznik = 1;
            }
            else
            {
                przelicznik = item.TWR_ILOSC_SZT.Value / item.TWR_ILOSC_OPAK.Value;
            }


            if (item.TWR_ILOSC_OPAK == 0)
            {
                item.TWR_ILOSC_OPAK = 60;
            }


            string zbiorczy = ">:" + item.TWR_SSCC + item.TWR_EAN_PALETY + item.TWR_CECHA + 1;
            string zbiorczynapis = item.TWR_SSCC + item.TWR_EAN_PALETY + item.TWR_CECHA + 1;







            string ZPLString = "^XA ^POI ^CF0,25^CI28^FO1,280^GB1540,1,3^FS " +
                               "^FO1,495^GB1540,1,3^FS " +
                               "^FO409,160^FH^FDp.los@pure-ice.pl^FS " +

                               "^FO160,160^FH^FDtel. 501 341 241^FS " +
                               "^FO140,127^F0^FD05-850 Ożarów Mazowiecki ul Poznańska 165^FS " +
                               "^FO100,70^FH^CF0,35^FDPure Ice Sp. Z.o.o^FS " +

                               "^FO30,220^F0^CF0,40^FD" + item.TWR_NAZWA + "^FS" +
                               "^CF0,15" +
                               "^FO400,220 ^FH ^FD" + item.TWR_ILOSC_OPAK + "x" + przelicznik + "=" +
                               item.TWR_ILOSC_SZT + "  ^FS" +

"^BY2,2,120 ^FT650,100 ^BCN,,N,N"+
                               "^FD"+item.TWR_EAN_OPAK+"^FS"+




                               "^FO50,300^FH^FDCONTENT / ZAWARTOSC ^FS ^" +
                               "^CF0,35" +
                               "^FO50,320^FH^FD" + item.TWR_EAN_OPAK + "^FS ^" +


                               "^CF0,15" +
                               "^FO590,300^FH^FDBATCH/ LOT / SERIA :^FS ^" +
                               "^CF0,35" +
                               "^FO590,320^FH^FD" + item.TWR_CECHA + "^FS ^" +

                               "^CF0,15" +
                               "^FO50,370^FH^FDBEST BEFORE / NAJLEPSZE DO: ^FS ^" +
                               "^CF0,35" +
                               "^FO50,390^FH^FD" + item.TWR_DATA + " (mm.yyyy)^FS ^" +

                               "^CF0,15" +
                               "^FO590,370^FH^FDCOUNT / LICZBA:^FS ^" +
                               "^CF0,35" +
                               "^FO590,390^FH^FD" + item.TWR_ILOSC_OPAK + "^FS ^" +

                               "^CF0,15" +
                               "^FO50,440^FH^FDSSCC: ^FS ^" +
                               "^CF0,35" +
                               "^FO50,460^FH^FD" + item.TWR_SSCC + "^FS ^" +

                               "^CF0,15" +
                               "^FO590,440^FH^FDPURCHASE FROM / KUPIONY OD:^FS" +
                               "^CF0,35" +
                               "^FO590,460^FH^FD5902596938000^FS ^" +



                               "^BY4,3,251 ^FT186,770 ^BCN,,N,N" +
                               "^FD" + pierwszy + "^FS" +
                               "^FT350,800 ^A0N,32,31 ^FH ^FD" + napispodpierwszym + "^FS" +

                               "^BY4,3,251 ^FT100,1070 ^BCN,,N,N" +
                               "^FD" + drugi + "^FS" +
                               "^FT400,1100 ^A0N,32,31 ^FH ^FD" + napispoddrugi + "^FS" +


                               "^BY4,3,251 ^FT230,1370 ^BCN,,N,N" +
                               "^FD" + trzeci + "^FS" +
                               "^FT400,1400 ^A0N,32,31 ^FH ^FD" + napispodtrzecim + "^FS" +
                               "^FO1,1470^GB1540,1,3^FS " +

                               "^BY2,3,160 ^FT50,1640 ^BCN,,N,N" +
                               "^FD" + zbiorczy + "^FS" +
                               "^FT165,1670 ^A0N,32,31 ^FH ^FD" + zbiorczynapis + "^FS" +



                               "^XZ";
            byte[] zpl = Encoding.UTF8.GetBytes(ZPLString);
            //            byte[] zpl = Encoding.UTF8.GetBytes(
            var request = (HttpWebRequest)WebRequest.Create("http://api.labelary.com/v1/printers/8dpmm/labels/5.2x8.2/");

            request.Method = "POST";
            request.Headers.Add("X-Page-Size", "A5");

            request.Accept = "application/pdf"; // omit this line to get PNG images back
            request.ContentType = "application/x-www-form-urlencoded";

            request.ContentLength = zpl.Length;

            var requestStream = request.GetRequestStream();
            requestStream.Write(zpl, 0, zpl.Length);
            requestStream.Close();
            DateTime dt = DateTime.Now;
            string data = String.Format("{0:s}", dt);

            string nazwa = "ETYKIETA_L" + item.ZLEC_LINE + "_NR_PALETY_" + item.NR_PALETY + "_" + data;

            string poprawionanazwa2 = "";
            for (int i = 0; i < nazwa.Length; i++)
            {

                if (nazwa[i] != ':')
                {
                    poprawionanazwa2 = poprawionanazwa2 + nazwa[i];
                }
                else
                {
                    poprawionanazwa2 = poprawionanazwa2 + "_";
                }
            }

            string path = @"C:\etykiety\PL\L" + item.ZLEC_LINE + "\\" + poprawionanazwa2;
            try
            {
                logger.Info("GENERUJE PDF ORAZ DRUKUJE NA DOMYSLNA DRUKARKE");
                var response = (HttpWebResponse)request.GetResponse();
                var responseStream = response.GetResponseStream();
                var fileStream = File.Create(path + ".pdf"); // change file name for PNG images
                responseStream.CopyTo(fileStream);
                responseStream.Close();
                fileStream.Close();
                ProcessStartInfo info = new ProcessStartInfo();
                info.Verb = "print";
                info.FileName = (path + ".pdf");
                info.CreateNoWindow = true;
                info.WindowStyle = ProcessWindowStyle.Hidden;

                Process p = new Process();
                p.StartInfo = info;
                p.Start();



            }
            catch (WebException e)
            {
                logger.Info("Error: {0}", e.Status);

            }



            //    doc.PrintDocument

            //         doc.PrintDocument.Print();

        }

    }
}

