﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace PureIce_Printer
{
  public  class MainWindowViewModel : INotifyPropertyChanged
    {

        private BackgroundWorker bw;
        private ObservableCollection<ZZSPE_PRINT> print;
        public ObservableCollection<ZZSPE_PRINT> Print
        {
            get => print; set
            {
                print = value;
                OnPropertyChanged();
            }
        }


        public void RefreshDataTable()
        {
            dataload data = new dataload();

          Print   = new ObservableCollection<ZZSPE_PRINT>(data.ListPrinter());
        }

        public void Listener()
        {
            bw = new BackgroundWorker();
            bw.WorkerSupportsCancellation = true;
            bw.DoWork += new DoWorkEventHandler(bw_DoWork);
            bw.RunWorkerAsync();
        }
        void bw_DoWork(object sender, DoWorkEventArgs e)

        {

            Print _print = new Print();
            _print.startprinting(true); ;



        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
