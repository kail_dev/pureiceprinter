﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PureIce_Printer
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindowViewModel viewModel { get; set; }
        public MainWindow()
        {

            InitializeComponent();

            var config = new NLog.Config.LoggingConfiguration();
            DateTime dt1 = DateTime.Now;
            string data = String.Format("{0:s}", dt1);

            var logfile = new NLog.Targets.FileTarget("logfile") { FileName = "logs\\etykiety_log" + data + ".txt" };
            var logconsole = new NLog.Targets.ConsoleTarget("logconsole");

            config.AddRule(LogLevel.Info, LogLevel.Fatal, logconsole);
            config.AddRule(LogLevel.Debug, LogLevel.Fatal, logfile);

            NLog.LogManager.Configuration = config;


            viewModel = new MainWindowViewModel();
            DataContext = viewModel;
            viewModel.RefreshDataTable();
            Start.IsEnabled = false; 
            Start.Content = "Uruchomiona";
        //    dataload dt = new dataload();
          //  var ss= dt.PrintData(63464);



         viewModel.Listener();
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            Start.IsEnabled = false;
            Start.Content = "Uruchomiona";
            
            viewModel.Listener();
        }

        private void refresh_Click(object sender, RoutedEventArgs e)
        {
            viewModel.RefreshDataTable();

        }

        private void Reprint_Click(object sender, RoutedEventArgs e)
        {
            ZZSPE_PRINT sp = Data.SelectedItem as ZZSPE_PRINT;
            sp.ID_PRINTER = 2;
            sp.STATUS = 0;
            dataload _dataload = new dataload();
            _dataload.Print(sp);

            viewModel.RefreshDataTable();
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            Start.IsEnabled = true;
            Print _print = new Print();
            _print.startprinting(false); ;
        }
    }
}
