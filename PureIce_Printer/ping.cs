﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace PureIce_Printer
{
 public   class ping
    {
        public virtual bool printer_status(string ipadress)
        {
            Ping myPing = new Ping();
            PingReply reply = myPing.Send(ipadress, 100);
            if (reply.Status == IPStatus.TimedOut)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
